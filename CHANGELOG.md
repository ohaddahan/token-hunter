# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 1.2.0 2020-07-08

- Add support for project-level searches with the `-p` arg.  Note that this is a breaking change for previous versions as the `-p` argument was reassigned from proxy argument.  The proxy argument can now be assigned with `-x`.
- Add support for CI job log searches.
- Performance enhancements.